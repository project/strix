$Id

This is the README for the Strix theme for Drupal
http://www.drupal.org/project/strix

### Overview ###

Strix is a theme specifically designed for the Drupal administration pages. It built as a sub theme of the Zen theme.

Some of the features in this theme:
- Clean and compact /admin overview page
- CSS based drop-down effects for the navigation menu block (see Configuration below)
- Fieldsets on the node edit and node add pages are in a column to the right.
- Fixed width for the front-end, fluid width for the admin pages.
- Inline help text is getting out of your way and has been placed below the actual form instead of above it.
- Primitively recolorable: just change the body background color…

And if you are wondering why it all looks so clean, that's because all form field descriptions are hidden… Currently through CSS, I'd like to make that a theme setting someday.


### Requirements ### 

Zen theme: http://www.drupal.org/project/zen


### Installation ###

Put the Strix theme folder in the location as Zen. Usually this is in sites/all/themes.


### Configuration ###

To get the most out of this theme you'll have to manually adjust some settings for the Navigation menu and move some of the menu items in there to a new menu as follows:

	1. Go to www.example.com/admin/build/menu-customize/navigation
	Check the 'expanded' checkbox for the these menu items:
	
	- Create content
	- Administer
	- Content management
	- Site building
	- Site configuration
	- User management
	- Reports
	
	These items need to be expanded for the dropdown effect in
	
	2. Create a new menu at www.example.com/admin/build/menu/add, name it something like user-menu. Save the menu
	
	3. Go back to www.example.com/admin/build/menu-customize/navigation to edit these links and move them to the new user menu, in this order:
	
	- My account
	- Help
	- Logout

	4. Enable the menu and make it show up in the header region on the blocks page: www.example.com/admin/build/block. 

This should give you a nice little menu along the top right of the page.


### Contact ###

Maintainer: Roy Scholten (yoroy) - http://drupal.org/user/41502

This theme has been sponsored by yoroy, a small interaction design studio from The Netherlands. Visit http://www.yoroy.com for more tasty design bits. 







