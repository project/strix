<?php

/**
 * @file
 *
 * OVERRIDING THEME FUNCTIONS
 *
 */
 
/**
 * Implementation of HOOK_theme().
 */
function strix_theme(&$existing, $type, $theme, $path) {
  return zen_theme($existing, $type, $theme, $path);
}

/**
 * Override or insert PHPTemplate variables into the block templates.
 *
 * @param $vars
 *   A sequential array of variables to pass to the theme template.
 * @param $hook
 *   The name of the theme function being called ("block" in this case.)
 */
function strix_preprocess_block(&$vars, $hook) {
  $block = $vars['block'];

  // Special classes for blocks
  $block_classes = array();
  $block_classes[] = 'block-' . $block->module;
  $block_classes[] = 'region-' . $vars['block_zebra'];
  $block_classes[] = $vars['zebra'];
  $block_classes[] = 'region-count-' . $vars['block_id'];
  $block_classes[] = 'count-' . $vars['id'];
  $vars['block_classes'] = implode(' ', $block_classes);

  $vars['edit_links'] = '';
  if (theme_get_setting('zen_block_editing') && user_access('administer blocks')) {
    // Display 'edit block' for custom blocks
    if ($block->module == 'block') {
      $edit_links[] = l('<span>' . t('B') . '</span>', 'admin/build/block/configure/' . $block->module . '/' . $block->delta,
        array(
          'attributes' => array(
            'title' => t('edit block content'),
            'class' => 'block-edit',
          ),
          'query' => drupal_get_destination(),
          'html' => TRUE,
        )
      );
    }
    // Display 'configure' for other blocks
    else {
      $edit_links[] = l('<span>' . t('C') . '</span>', 'admin/build/block/configure/' . $block->module . '/' . $block->delta,
        array(
          'attributes' => array(
            'title' => t('configure block'),
            'class' => 'block-config',
          ),
          'query' => drupal_get_destination(),
          'html' => TRUE,
        )
      );
    }

    // Display 'administer views' for views blocks
    if ($block->module == 'views' && user_access('administer views')) {
      $edit_links[] = l('<span>' . t('V') . '</span>', 'admin/build/views/' . $block->delta . '/edit',
        array(
          'attributes' => array(
            'title' => t('edit view'),
            'class' => 'block-edit-view',
          ),
          'query' => drupal_get_destination(),
          'fragment' => 'edit-block',
          'html' => TRUE,
        )
      );
    }
    // Display 'edit menu' for menu blocks
    elseif (($block->module == 'menu' || ($block->module == 'user' && $block->delta == 1)) && user_access('administer menu')) {
      $menu_name = ($block->module == 'user') ? 'navigation' : $block->delta;
      $edit_links[] = l('<span>' . t('M') . '</span>', 'admin/build/menu-customize/' . $menu_name,
        array(
          'attributes' => array(
            'title' => t('edit menu'),
            'class' => 'block-edit-menu',
          ),
          'query' => drupal_get_destination(),
          'html' => TRUE,
        )
      );
    }
    $vars['edit_links_array'] = $edit_links;
    $vars['edit_links'] = '<div class="edit">' . implode(' ', $edit_links) . '</div>';
  }
}


/**
 * This function formats an administrative page for viewing.
 *
 * @param $blocks
 *   An array of blocks to display. Each array should include a
 *   'title', a 'description', a formatted 'content' and a
 *   'position' which will control which container it will be
 *   in. This is usually 'left' or 'right'.
 * @ingroup themeable
 */
function strix_admin_page($blocks) {
  $output = '<div class="admin clear-block">';

  $output .= '<div class="compact-link">';
  if (system_admin_compact_mode()) {
    $output .= l(t('Show descriptions'), 'admin/compact/off', array('attributes' => array('title' => t('Expand layout to include descriptions.'))));
  }
  else {
    $output .= l(t('Hide descriptions'), 'admin/compact/on', array('attributes' => array('title' => t('Compress layout by hiding descriptions.'))));
  }
  $output .= '</div>';

  foreach ($blocks as $block) {
    if ($block_output = theme('admin_block', $block)) {
      $output .= $block_output;
    }
  }
  $output .= '</div>';

  return $output;
}



function strix_node_form($form) {
  $output = "\n<div class=\"node-form\">\n";

  // Admin form fields and submit buttons must be rendered first, because
  // they need to go to the bottom of the form, and so should not be part of
  // the catch-all call to drupal_render().
  $admin = '';
  if (isset($form['options'])) {
    $admin .= "    <div class=\"options-form\">\n";
    $admin .= drupal_render($form['options']);
    $admin .= "    </div>\n";
  }
  if (isset($form['menu'])) {
    $admin .= "    <div class=\"menu-item-form\">\n";
    $admin .= drupal_render($form['menu']);
    $admin .= "    </div>\n";
  }
  if (isset($form['comment_settings'])) {
    $admin .= "    <div class=\"comment-form\">\n";
    $admin .= drupal_render($form['comment_settings']);
    $admin .= "    </div>\n";
  }
  if (isset($form['author'])) {
    $admin .= "    <div class=\"authored\">\n";
    $admin .= drupal_render($form['author']);
    $admin .= "    </div>\n";
  }
  if (isset($form['revision_information'])) {
    $admin .= "    <div class=\"revision-form\">\n";
    $admin .= drupal_render($form['revision_information']);
    $admin .= "    </div>\n";
  }
  if (isset($form['book'])) {
    $admin .= "    <div class=\"book-form\">\n";
    $admin .= drupal_render($form['book']);
    $admin .= "    </div>\n";
  }
//  if (isset($form['attachments'])) {
//    $admin .= "    <div class=\"attachments-form\">\n";
//    $admin .= drupal_render($form['attachments']);
//    $admin .= "    </div>\n";
//  }


  $standardplus = '';
  if (isset($form['taxonomy'])) {
    $standardplus .= "    <div class=\"taxonomy\">\n";
    $standardplus .= drupal_render($form['taxonomy']);
    $standardplus .= "    </div>\n";
  }

  $buttons = drupal_render($form['buttons']);


  // Everything else gets rendered here, and is displayed before the admin form
  // field and the submit buttons.
  $output .= "  <div class=\"standard\">\n";
  $output .= drupal_render($form);
  $output .= $standardplus;
  $output .= "<div class=\"submits\">\n";
  $output .= $buttons;
  $output .= "  </div>\n";
  $output .= "  </div>\n";

  if (!empty($admin)) {
    $output .= "  <div class=\"admin\">\n";
    $output .= "  <div class=\"admin-inner\">\n";
    $output .= $admin;
    $output .= "  </div>\n";
    $output .= "  </div>\n";
  }
  $output .= "</div>\n";

  return $output;
}


/**
* Need some specific classes for just the admin block.
*/
function strix_admin_block($block) {
  // Don't display the block if it has no content to display.
  if (empty($block['content'])) {
    return '';
  }
  $admintitle = strtolower(str_replace(' ', '-', $block[title]));  

  $output = <<< EOT
  <div class="admin-panel">
  <div class="admin-panel-inner $admintitle">
  
    <h3>
      $block[title]
    </h3>
    <div class="description">
        $block[description]
      </div>
    <div class="body">
      $block[content]
    </div>
  </div>
  </div>
EOT;
  return $output;
}


/**
 * Duplicate of theme_menu_local_tasks() but adds clear-block to tabs.
 */
function strix_menu_local_tasks() {
  $output = '';
  if ($primary = menu_primary_local_tasks()) {
    $output .= '<ul class="tabs primary clear-block">' . $primary . '</ul>';
  }
  if ($secondary = menu_secondary_local_tasks()) {
    $output .= '<ul class="tabs secondary clear-block">' . $secondary . '</ul>';
  }

  return $output;
}


/**
 * Duplicate of theme_menu_local_tasks() but adds clear-block to tabs.
 */
function strix_menu_item($link, $has_children, $menu = '', $in_active_trail = FALSE, $extra_class = NULL) {
  $class = ($menu ? 'expanded' : ($has_children ? 'collapsed' : 'leaf'));
  if (!empty($extra_class)) {
    $class .= ' '. $extra_class;
  }
  if ($in_active_trail) {
    $class .= ' active-trail';
  }
  $id = strtolower(str_replace(' ', '-', strip_tags($link)));  
  $id = preg_replace("/[^a-zA-Z0-9]/", "", strip_tags($link));
  return '<li id="'.$id.'" class="'. $class .'">'. $link . $menu ."</li>\n";
}


/**
 * Theme status update page.
 */
function strix_update_version($version, $tag, $class) {
  $output = '';
  $output .= '<table class="version '. $class .'">';
  $output .= '<tr>';
  $output .= '<td class="version-title">'. $tag ."</td>\n";
  $output .= '<td class="version-details">';
  $output .= l($version['version'], $version['download_link']);
  $output .= ' <span class="version-date">('. format_date($version['date'], 'custom', 'Y-M-d') .')</span>';
  $output .= "</td>\n";
  $output .= '<td class="version-links">';
  $links = array();
  $links['update-release-notes'] = array(
    'title' => t('Release notes'),
    'href' => $version['release_link'],
  );
  $output .= theme('links', $links);
  $output .= '</td>';
  $output .= '</tr>';
  $output .= "</table>\n";
  return $output;
}


/**
* Slightly modified version of how the available updates page is rendered.
*/
function strix_update_report($data) {
  $last = variable_get('update_last_check', 0);
  $output = '<div class="update checked">'. ($last ? t('Last checked: @time ago', array('@time' => format_interval(time() - $last))) : t('Last checked: never'));
  $output .= ' <span class="check-manually">('. l(t('Check manually'), 'admin/reports/updates/check') .')</span>';
  $output .= "</div>\n";

  if (!is_array($data)) {
    $output .= '<p>'. $data .'</p>';
    return $output;
  }

  $header = array();
  $rows = array();

  $notification_level = variable_get('update_notification_threshold', 'all');

  foreach ($data as $project) {
    switch ($project['status']) {
      case UPDATE_CURRENT:
        $class = 'ok';
        $icon = theme('image', (path_to_theme()) . '/images/messages-status.png', t('ok'), t('ok'));
        break;
      case UPDATE_UNKNOWN:
        $class = 'unknown';
        $icon = theme('image', (path_to_theme()) . '/images/messages-warning.png', t('warning'), t('warning'));
        break;
      case UPDATE_NOT_SECURE:
      case UPDATE_REVOKED:
      case UPDATE_NOT_SUPPORTED:
        $class = 'error';
        $icon = theme('image', (path_to_theme()) . '/images/messages-error.png', t('error'), t('error'));
        break;
      case UPDATE_NOT_CHECKED:
      case UPDATE_NOT_CURRENT:
      default:
        $class = 'warning';
        $icon = theme('image', (path_to_theme()) . '/images/messages-warning.png', t('warning'), t('warning'));
        break;
    }

    $row = '<div class="project">';
    if (isset($project['title'])) {
      if (isset($project['link'])) {
        $row .= l($project['title'], $project['link']);
      }
      else {
        $row .= check_plain($project['title']);
      }
    }
    else {
      $row .= check_plain($project['name']);
    }
    /* adding a class for the existing_version */
    $row .= ' <span class="version-existing">'. check_plain($project['existing_version']) . '</span>';
    if ($project['install_type'] == 'dev' && !empty($project['datestamp'])) {
      $row .= ' <span class="version-date">('. format_date($project['datestamp'], 'custom', 'Y-M-d') .')</span>';
    }
    $row .= "</div>\n";

    $row .= "<div class=\"versions\">\n";
    $row .= '<div class="version-status">';
    $row .= '<span class="icon">'. $icon .'</span>';
    switch ($project['status']) {
      case UPDATE_NOT_SECURE:
        $row .= '<span class="security-error">'. t('Security update required!') .'</span>';
        break;
      case UPDATE_REVOKED:
        $row .= '<span class="revoked">'. t('Revoked!') .'</span>';
        break;
      case UPDATE_NOT_SUPPORTED:
        $row .= '<span class="not-supported">'. t('Not supported!') .'</span>';
        break;
      case UPDATE_NOT_CURRENT:
        $row .= '<span class="not-current">'. t('Update available') .'</span>';
        break;
      case UPDATE_CURRENT:
        $row .= '<span class="current">'. t('Up to date') .'</span>';
        break;
      default:
        $row .= check_plain($project['reason']);
        break;
    }
    $row .= "</div>\n";


    if (isset($project['recommended'])) {
      if ($project['status'] != UPDATE_CURRENT || $project['existing_version'] !== $project['recommended']) {

        // First, figure out what to recommend.
        // If there's only 1 security update and it has the same version we're
        // recommending, give it the same CSS class as if it was recommended,
        // but don't print out a separate "Recommended" line for this project.
        if (!empty($project['security updates']) && count($project['security updates']) == 1 && $project['security updates'][0]['version'] === $project['recommended']) {
          $security_class = ' version-recommended version-recommended-strong';
        }
        else {
          $security_class = '';
          $version_class = 'version-recommended';
          // Apply an extra class if we're displaying both a recommended
          // version and anything else for an extra visual hint.
          if ($project['recommended'] !== $project['latest_version']
              || !empty($project['also'])
              || ($project['install_type'] == 'dev'
                 && isset($project['dev_version'])
                 && $project['latest_version'] !== $project['dev_version']
                 && $project['recommended'] !== $project['dev_version'])
              || (isset($project['security updates'][0])
                 && $project['recommended'] !== $project['security updates'][0])
              ) {
            $version_class .= ' version-recommended-strong';
          }
          $row .= theme('update_version', $project['releases'][$project['recommended']], t('Recommended update:'), $version_class);
        }

        // Now, print any security updates.
        if (!empty($project['security updates'])) {
          foreach ($project['security updates'] as $security_update) {
            $row .= theme('update_version', $security_update, t('Security update:'), 'version-security'. $security_class);
          }
        }
      }

      if ($project['recommended'] !== $project['latest_version']) {
        $row .= theme('update_version', $project['releases'][$project['latest_version']], t('Latest version:'), 'version-latest');
      }
      if ($project['install_type'] == 'dev'
          && $project['status'] != UPDATE_CURRENT
          && isset($project['dev_version'])
          && $project['recommended'] !== $project['dev_version']) {
        $row .= theme('update_version', $project['releases'][$project['dev_version']], t('Development version:'), 'version-latest');
      }
    }

    if (isset($project['also'])) {
      foreach ($project['also'] as $also) {
        $row .= theme('update_version', $project['releases'][$also], t('Also available:'), 'version-also-available');
      }
    }

    $row .= "</div>\n"; // versions div.

    $row .= "<div class=\"info\">\n";
    if (!empty($project['extra'])) {
      $row .= '<div class="extra">'."\n";
      foreach ($project['extra'] as $key => $value) {
        $row .= '<div class="'. $value['class'] .'">';
        $row .= check_plain($value['label']) .': ';
        $row .= theme('placeholder', $value['data']);
        $row .= "</div>\n";
      }
      $row .= "</div>\n";  // extra div.
    }

    $row .= '<div class="includes">';
    sort($project['includes']);
    $row .= t('Includes: %includes', array('%includes' => implode(', ', $project['includes'])));
    $row .= "</div>\n";

    $row .= "</div>\n"; // info div.

    if (!isset($rows[$project['project_type']])) {
      $rows[$project['project_type']] = array();
    }
    $rows[$project['project_type']][] = array(
      'class' => $class,
      'data' => array($row),
    );
  }

  $project_types = array(
    'core' => t('Drupal core'),
    'module' => t('Modules'),
    'theme' => t('Themes'),
    'disabled-module' => t('Disabled modules'),
    'disabled-theme' => t('Disabled themes'),
  );
  foreach ($project_types as $type_name => $type_label) {
    if (!empty($rows[$type_name])) {
      $output .= "\n<h3>". $type_label ."</h3>\n";
      $output .= theme('table', $header, $rows[$type_name], array('class' => 'update'));
    }
  }
  drupal_add_css(drupal_get_path('module', 'update') .'/update.css');
  return $output;
}